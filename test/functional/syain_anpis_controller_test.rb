require 'test_helper'

class SyainAnpisControllerTest < ActionController::TestCase
  setup do
    @syain_anpi = syain_anpis(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:syain_anpis)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create syain_anpi" do
    assert_difference('SyainAnpi.count') do
      post :create, syain_anpi: { genzaiti: @syain_anpi.genzaiti, idouhantei: @syain_anpi.idouhantei, lat: @syain_anpi.lat, lng: @syain_anpi.lng, recieveData: @syain_anpi.recieveData, recieveTime: @syain_anpi.recieveTime, sousalog: @syain_anpi.sousalog, syain_info_id: @syain_anpi.syain_info_id, syussyabi: @syain_anpi.syussyabi }
    end

    assert_redirected_to syain_anpi_path(assigns(:syain_anpi))
  end

  test "should show syain_anpi" do
    get :show, id: @syain_anpi
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @syain_anpi
    assert_response :success
  end

  test "should update syain_anpi" do
    put :update, id: @syain_anpi, syain_anpi: { genzaiti: @syain_anpi.genzaiti, idouhantei: @syain_anpi.idouhantei, lat: @syain_anpi.lat, lng: @syain_anpi.lng, recieveData: @syain_anpi.recieveData, recieveTime: @syain_anpi.recieveTime, sousalog: @syain_anpi.sousalog, syain_info_id: @syain_anpi.syain_info_id, syussyabi: @syain_anpi.syussyabi }
    assert_redirected_to syain_anpi_path(assigns(:syain_anpi))
  end

  test "should destroy syain_anpi" do
    assert_difference('SyainAnpi.count', -1) do
      delete :destroy, id: @syain_anpi
    end

    assert_redirected_to syain_anpis_path
  end
end
