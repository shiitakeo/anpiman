require 'test_helper'

class SyainInfosControllerTest < ActionController::TestCase
  setup do
    @syain_info = syain_infos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:syain_infos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create syain_info" do
    assert_difference('SyainInfo.count') do
      post :create, syain_info: { busyo: @syain_info.busyo, deviceID: @syain_info.deviceID, name: @syain_info.name, syainID: @syain_info.syainID, syainanpi_id: @syain_info.syainanpi_id, zyuusyo: @syain_info.zyuusyo }
    end

    assert_redirected_to syain_info_path(assigns(:syain_info))
  end

  test "should show syain_info" do
    get :show, id: @syain_info
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @syain_info
    assert_response :success
  end

  test "should update syain_info" do
    put :update, id: @syain_info, syain_info: { busyo: @syain_info.busyo, deviceID: @syain_info.deviceID, name: @syain_info.name, syainID: @syain_info.syainID, syainanpi_id: @syain_info.syainanpi_id, zyuusyo: @syain_info.zyuusyo }
    assert_redirected_to syain_info_path(assigns(:syain_info))
  end

  test "should destroy syain_info" do
    assert_difference('SyainInfo.count', -1) do
      delete :destroy, id: @syain_info
    end

    assert_redirected_to syain_infos_path
  end
end
