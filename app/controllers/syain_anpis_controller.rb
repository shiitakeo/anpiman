class SyainAnpisController < ApplicationController
  # GET /syain_anpis
  # GET /syain_anpis.json
  def index
    @syain_anpis = SyainAnpi.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @syain_anpis }
    end
  end

  # GET /syain_anpis/1
  # GET /syain_anpis/1.json
  def show
    @syain_anpi = SyainAnpi.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @syain_anpi }
    end
  end

  # GET /syain_anpis/new
  # GET /syain_anpis/new.json
  def new
    @syain_anpi = SyainAnpi.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @syain_anpi }
    end
  end

  # GET /syain_anpis/1/edit
  def edit
    @syain_anpi = SyainAnpi.find(params[:id])
  end

  # POST /syain_anpis
  # POST /syain_anpis.json
  def create
    @syain_anpi = SyainAnpi.new(params[:syain_anpi])

    respond_to do |format|
      if @syain_anpi.save
        format.html { redirect_to @syain_anpi, notice: 'Syain anpi was successfully created.' }
        format.json { render json: @syain_anpi, status: :created, location: @syain_anpi }
      else
        format.html { render action: "new" }
        format.json { render json: @syain_anpi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /syain_anpis/1
  # PUT /syain_anpis/1.json
  def update
    @syain_anpi = SyainAnpi.find(params[:id])

    respond_to do |format|
      if @syain_anpi.update_attributes(params[:syain_anpi])
        format.html { redirect_to @syain_anpi, notice: 'Syain anpi was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @syain_anpi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /syain_anpis/1
  # DELETE /syain_anpis/1.json
  def destroy
    @syain_anpi = SyainAnpi.find(params[:id])
    @syain_anpi.destroy

    respond_to do |format|
      format.html { redirect_to syain_anpis_url }
      format.json { head :no_content }
    end
  end
end
