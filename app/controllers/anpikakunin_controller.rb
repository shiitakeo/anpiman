class AnpikakuninController < ApplicationController
  skip_before_filter :verify_authenticity_token ,:only=>[:new]
  def regist
    puts "------ anpi regist ------"
    if SyainAnpi.find_by_syain_info_id(params[:syachiku_no]).nil?
      anpi = SyainAnpi.new
      anpi.syain_info_id = params[:syachiku_no]
      anpi.syussyabi = params[:syukin_date]
      anpi.lat = params[:latitude]
      anpi.lng = params[:longitude]
      anpi.recieveData = Date.today
      anpi.recieveTime = Time.now
      anpi.save
      logger.info(params)
    else
      anpi = SyainAnpi.find_by_syain_info_id(params[:syachiku_no])
      anpi.syain_info_id = params[:syachiku_no]
      anpi.lat = params[:latitude]
      anpi.lng = params[:longitude]
      anpi.syussyabi = params[:syukin_date]
      anpi.recieveData = Date.today
      anpi.recieveTime = Time.now
      anpi.save
    end
  end

  def auto
    if SyainAnpi.find_by_syain_info_id(params[:syachiku_no]).nil?
      anpi = SyainAnpi.new
      anpi.syain_info_id = params[:syachiku_no]
      anpi.lat = params[:latitude]
      anpi.lng = params[:longitude]
      zyuusyo = Geokit::Geocoders::GoogleGeocoder.reverse_geocode sprintf("%f,%f",anpi.lat,anpi.lng)
      anpi.genzaiti = zyuusyo.full_address
      anpi.recieveData = Date.today
      anpi.recieveTime = Time.now
      anpi.save
    else
      anpi = SyainAnpi.find_by_syain_info_id(params[:syachiku_no])
      anpi.syain_info_id = params[:syachiku_no]
      anpi.lat = params[:latitude]
      anpi.lng = params[:longitude]
      zyuusyo = Geokit::Geocoders::GoogleGeocoder.reverse_geocode sprintf("%f,%f",anpi.lat,anpi.lng)
      anpi.genzaiti = zyuusyo.full_address
      anpi.recieveData = Date.today
      anpi.recieveTime = Time.now
      anpi.save
    end
  end

  def sousalog
    puts "******* sousaLog ********"
    if SyainAnpi.find_by_syain_info_id(params[:syachiku_no]).nil?
      anpi = SyainAnpi.new
      anpi.sousalog = 1
      anpi.save
    else
      anpi = SyainAnpi.find_by_syain_info_id(params[:syachiku_no])
      anpi.sousalog = 1
      anpi.save
    end
  end

  def resend
   require 'net/https'
   gcm_host = "android.googleapis.com"
   gcm_path = "/gcm/send"
   #api_key = "AIzaSyAFWihA-3TYTzjQkEjXDtoaSuc9CCu9hDU"
   api_key = "AIzaSyBQFxyhO_VtFo-obUx4hyNEmqXsA2vLyRg"

   @syain_infos = SyainInfo.all
   @anpi_infos = SyainAnpi.all
   @syain_infos.each do |syain_info|


     if syain_info.deviceID.nil?
     else
       if SyainAnpi.find_by_syain_info_id(syain_info.syainID).nil?
         puts "** send **"
         puts syain_info.syainID
         puts syain_info.deviceID
         reg_id = syain_info.deviceID
  
          message = { 
            "registration_ids" => [reg_id],
            "collapse_key" => "collapse_key",
            "delay_while_idle" => false,
            "time_to_live" => 60, 
            "data" => { "message" => "GCM Demo",
                        "detail" => "Hello world"}
          }
          
          http = Net::HTTP.new("android.googleapis.com", 443);
          http.use_ssl = true
          http.start{|w|
            response = w.post(gcm_path,
              message.to_json + "\n",
              {"Content-Type" => "application/json",
               "Authorization" => "key=#{api_key}"})
            puts "response code = #{response.code}"
            puts "response body = #{response.body}"
          }
       else 
         anpi = SyainAnpi.find_by_syain_info_id(syain_info.syainID)
         if anpi.syussyabi.nil?
         puts "** send **"
         puts syain_info.syainID
         puts syain_info.deviceID
         reg_id = syain_info.deviceID
  
          message = { 
            "registration_ids" => [reg_id],
            "collapse_key" => "collapse_key",
            "delay_while_idle" => false,
            "time_to_live" => 60, 
            "data" => { "message" => "GCM Demo",
                        "detail" => "Hello world"}
          }
          
          http = Net::HTTP.new("android.googleapis.com", 443);
          http.use_ssl = true
          http.start{|w|
            response = w.post(gcm_path,
              message.to_json + "\n",
              {"Content-Type" => "application/json",
               "Authorization" => "key=#{api_key}"})
            puts "response code = #{response.code}"
            puts "response body = #{response.body}"
          }

         else
           puts "-- skip --"
           puts syain_info.syainID
         end
       end 
      end
    end
  end



  def new
   require 'net/https'
   gcm_host = "android.googleapis.com"
   gcm_path = "/gcm/send"
  # api_key = "AIzaSyAFWihA-3TYTzjQkEjXDtoaSuc9CCu9hDU"
   api_key = "AIzaSyBQFxyhO_VtFo-obUx4hyNEmqXsA2vLyRg"

   @syain_infos = SyainInfo.all
   @syain_infos.each do |syain_info|
     if syain_info.deviceID.nil?
     else
       reg_id = syain_info.deviceID
       puts " *** send ***"
       puts syain_info.deviceID
       puts syain_info.syainID
       puts syain_info.name
  
        message = { 
          "registration_ids" => [reg_id],
          "collapse_key" => "collapse_key",
          "delay_while_idle" => false,
          "time_to_live" => 60, 
          "data" => { "message" => "GCM Demo",
                      "detail" => "Hello world"}
        }
        
        http = Net::HTTP.new("android.googleapis.com", 443);
        http.use_ssl = true
        http.start{|w|
          response = w.post(gcm_path,
            message.to_json + "\n",
            {"Content-Type" => "application/json",
             "Authorization" => "key=#{api_key}"})
          puts "response code = #{response.code}"
          puts "response body = #{response.body}"
        }
  
      end
   end
  end
end
