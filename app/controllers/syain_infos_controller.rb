class SyainInfosController < ApplicationController
  # GET /syain_infos
  # GET /syain_infos.json
  def index
    @syain_infos = SyainInfo.all
    @syain_anpis = SyainAnpi.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @syain_infos }
    end
  end

  # GET /syain_infos/1
  # GET /syain_infos/1.json
  def show
    @syain_info = SyainInfo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @syain_info }
    end
  end

  # GET /syain_infos/new
  # GET /syain_infos/new.json
  def new
    @syain_info = SyainInfo.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @syain_info }
    end
  end

  # GET /syain_infos/1/edit
  def edit
    @syain_info = SyainInfo.find(params[:id])
  end

  # POST /syain_infos
  # POST /syain_infos.json
  def create
    @syain_info = SyainInfo.new(params[:syain_info])

    respond_to do |format|
      if @syain_info.save
        format.html { redirect_to @syain_info, notice: 'Syain info was successfully created.' }
        format.json { render json: @syain_info, status: :created, location: @syain_info }
      else
        format.html { render action: "new" }
        format.json { render json: @syain_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /syain_infos/1
  # PUT /syain_infos/1.json
  def update
    @syain_info = SyainInfo.find(params[:id])

    respond_to do |format|
      if @syain_info.update_attributes(params[:syain_info])
        format.html { redirect_to @syain_info, notice: 'Syain info was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @syain_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /syain_infos/1
  # DELETE /syain_infos/1.json
  def destroy
    @syain_info = SyainInfo.find(params[:id])
    @syain_info.destroy

    respond_to do |format|
      format.html { redirect_to syain_infos_url }
      format.json { head :no_content }
    end
  end
end
