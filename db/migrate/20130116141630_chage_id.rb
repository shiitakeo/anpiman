class ChageId < ActiveRecord::Migration
  def up
    change_column :syain_anpis, :syain_info_id, :string
  end

  def down
    change_column :syain_anpis, :syain_info_id, :integer
  end
end
