class CreateSyainAnpis < ActiveRecord::Migration
  def change
    create_table :syain_anpis do |t|
      t.integer :syain_info_id
      t.string :syussyabi
      t.string :lng
      t.string :lat
      t.string :genzaiti
      t.string :idouhantei
      t.string :sousalog
      t.time :recieveTime
      t.date :recieveData

      t.timestamps
    end
  end
end
