class CreateSyainInfos < ActiveRecord::Migration
  def change
    create_table :syain_infos do |t|
      t.string :syainID
      t.string :name
      t.string :busyo
      t.string :zyuusyo
      t.text :deviceID
      t.integer :syainanpi_id

      t.timestamps
    end
  end
end
